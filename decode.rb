# converts a hex string into a human-readable script
def decode(hex)
	puts "decoding..."
	tokens = tokenize(hex)
	puts tokens.join(" ")
end

# converts the hex string into an array of tokens
def tokenize(hex)
	puts "tokenizing..."
	tokens = []
	bytes = []

	# Each byte is two hex digits
	hex.chars.each_slice(2) do |hex_pair|
		bytes << hex_pair.join
	end

	# iterate through each byte
	i = 0
	while i < bytes.size do
		byte = bytes[i]
		opcode = OPCODES[byte]

		if opcode != nil
			tokens << opcode

			num = byte.to_i(16)
			if num <= 75 && num >= 1
				# these special opcodes mean push num bytes onto stack
				# in this case, we should just create a token that contains the next num bytes
				tokens << data_bytes(i, num, bytes)
				i += num
			end

			if opcode == "OP_PUSHDATA1"
				# The next byte contains the number of bytes to be pushed onto the stack.
				# convert the next byte to decimal
				numbytes = bytes[i+1]
				tokens << numbytes
				num = numbytes.to_i(16)
				i+=1
				tokens << data_bytes(i, num, bytes)
				i+=num
			elsif opcode == "OP_PUSHDATA2"
				# The next two bytes contain the number of bytes to be pushed onto the stack.
				# convert the next 2 bytes to decimal
				numbytes = bytes[i+1] << bytes[i+2]
				tokens << numbytes
				num = numbytes.to_i(16)
				i+= 2
				tokens << data_bytes(i, num, bytes)
				i+=num
			elsif opcode == "OP_PUSHDATA4"
				# The next four bytes contain the number of bytes to be pushed onto the stack.
				# convert the next 4 bytes to decimal
				numbytes = bytes[i+1] << bytes[i+2] << bytes[i+3] << bytes[i+4]
				tokens << numbytes
				num = numbytes.to_i(16)
				i+= 4
				tokens << data_bytes(i, num, bytes)
				i+=num
			elsif opcode == "OP_RETURN"
				# Any remaining bytes should be considered metadata and given their own token
				data = ""
				while i < bytes.size-1 do
					data << bytes[i+1]
					i+=1
				end
				tokens << data
			end
		else
			puts "Unrecognized opcode: " << byte
			break
		end
		i+=1
	end
	return tokens
end

# returns num bytes out of bytes starting at i
# raises an exception if i+num > bytes.size
def data_bytes(i, num, bytes)
	target = i + num
	data = ""
	while i < target do
		i+=1
		if i > bytes.size-1 
			raise "Cannot push " << num.to_s << " bytes onto stack. Ran out of bytes."
		end
		data << bytes[i]
	end
	return data
end