Utilities for encoding bitcoin script code to and from hex
-----------------------------------------------------------

Usage:

```
ruby script.rb decode 76a9143647ceb2fdec47d3594c6653eda85e534794a49a88ac
```

And to encode...

```
ruby script.rb encode "OP_DUP OP_HASH160 14 3647ceb2fdec47d3594c6653eda85e534794a49a OP_EQUALVERIFY OP_CHECKSIG"
```