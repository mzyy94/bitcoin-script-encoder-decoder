# TODO: implement this
def encode(str)
	puts "encoding..."

	tokens = str.split(' ')

	rev_opcodes = OPCODES.invert

	hex = []
	for token in tokens
		if(rev_opcodes[token])
			hex << rev_opcodes[token]
		else
			length = token.length
			if(length <= 75)
				hex << length
				hex << token
			elsif(length > 75 && length <= 256) 
				hex << '4c'
				hex << length.to_s(16)
				hex << token
			elsif(length > 256 && length <= 65536) 
				hex << '4d'
				hex << length.to_s(16)
				hex << token
			elsif(length > 65536 && length <= 4294967296) 
				hex << '4e'
				hex << length.to_s(16)
				hex << token
			end
		end
	end

	puts hex.join("")

end